﻿using System.Data.Entity;
using Core.Domain;

namespace Persistence
{
    public class PlutoContext : DbContext
    {
        public PlutoContext(): base("name=PlutoContext")
        {
            Configuration.LazyLoadingEnabled = false;
        }

        public virtual DbSet<Company> Companies { get; set; }
    }
}
