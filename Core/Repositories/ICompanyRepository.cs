﻿using System;
using Core.Domain;

namespace Core.Repositories
{
    public interface ICompanyRepository : IRepository<Company>
    {

    }
}
