﻿using System;
using Core.Repositories;

namespace Core
{
    public interface IUnitOfWork : IDisposable
    {
        ICompanyRepository Companies { get; }
        int Complete();
    }
}
