﻿namespace Core.Domain
{
    public class Company
    {
        public Company()
        {
            
        }

        public int Id { get; set; }

        public string Name { get; set; }
    }
}